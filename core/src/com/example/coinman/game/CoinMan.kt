package com.example.coinman.game

import com.badlogic.gdx.ApplicationAdapter
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.math.Intersector
import com.badlogic.gdx.math.Rectangle
import java.util.*

class CoinMan : ApplicationAdapter() {
    var batch: SpriteBatch? = null
    var background: Texture? = null
    private var man: MutableList<Texture> = mutableListOf()
    private var dizzy: Texture? = null
    var manState: Int = 0
    var pause: Int = 0
    var gravity: Float = 0.4f
    var velocity: Float = 0f
    var manY: Float = 0f
    lateinit var random: Random
    lateinit var manRectangle: Rectangle

    var coinXs: MutableList<Int> = mutableListOf()
    var coinYs: MutableList<Int> = mutableListOf()
    var coinRectangles: MutableList<Rectangle> = mutableListOf()
    var coin: Texture? = null
    var coinCount = 0

    var bombXs: MutableList<Int> = mutableListOf()
    var bombYs: MutableList<Int> = mutableListOf()
    var bombRectangles: MutableList<Rectangle> = mutableListOf()
    var bomb: Texture? = null
    var bombCount = 0

    var score = 0
    lateinit var font: BitmapFont
    var gameState = 0

    override fun create() {
        batch = SpriteBatch()
        background = Texture("bg.png")

        man.add(Texture("frame-1.png"))
        man.add(Texture("frame-2.png"))
        man.add(Texture("frame-3.png"))
        man.add(Texture("frame-4.png"))

        manY = (Gdx.graphics.height / 2).toFloat()

        coin = Texture("coin.png")
        bomb = Texture("bomb.png")
        dizzy = Texture("dizzy-1.png")

        random = Random()
        font = BitmapFont()
        font.color = Color.WHITE
        font.data.setScale(10F)
    }

    fun makeCoin() {
        val height = random.nextFloat() * Gdx.graphics.height
        coinYs.add(height.toInt())
        coinXs.add(Gdx.graphics.width)
    }

    fun makeBomb() {
        val height = random.nextFloat() * Gdx.graphics.height
        bombYs.add(height.toInt())
        bombXs.add(Gdx.graphics.width)
    }

    override fun render() {
        batch?.begin()
        batch?.draw(background, 0f, 0f, Gdx.graphics.width.toFloat(), Gdx.graphics.height.toFloat())

        if (gameState == 1) { //Playing
            if (bombCount < 400) {
                bombCount++
            } else {
                bombCount = 0
                makeBomb()
            }
            bombRectangles.clear()
            for (i in bombXs.indices) {
                batch?.draw(bomb, bombXs[i].toFloat(), bombYs[i].toFloat())
                bombXs[i] = bombXs[i] - 8
                bombRectangles.add(Rectangle(bombXs[i].toFloat(), bombYs[i].toFloat(), bomb!!.width.toFloat(), bomb!!.height.toFloat()))
            }

            if (coinCount < 125) {
                coinCount++
            } else {
                coinCount = 0
                makeCoin()
            }
            coinRectangles.clear()
            for (i in coinXs.indices) {
                batch?.draw(coin, coinXs[i].toFloat(), coinYs[i].toFloat())
                coinXs[i] = coinXs[i] - 4
                coinRectangles.add(Rectangle(coinXs[i].toFloat(), coinYs[i].toFloat(), coin!!.width.toFloat(), coin!!.height.toFloat()))
            }

            if (Gdx.input.justTouched()) {
                velocity = -17f
            }
            if (pause < 6) {
                pause++
            } else {
                pause = 0
                if (manState < 3) {
                    manState++
                } else {
                    manState = 0
                }
            }
            velocity += gravity
            manY -= velocity
            if (manY <= 0) {
                manY = 0f
            }
        } else if (gameState == 0) {
            //Waiting
            if (Gdx.input.justTouched()) {
                gameState = 1
            }
        } else if (gameState == 2) { //Game Over
            if (Gdx.input.justTouched()) {
                gameState = 1
                manY = (Gdx.graphics.height / 2).toFloat()
                score = 0
                velocity = 0f
                coinXs.clear()
                coinYs.clear()
                coinRectangles.clear()
                coinCount = 0
                bombXs.clear()
                bombYs.clear()
                bombRectangles.clear()
                bombCount = 0
            }
        }

        if (gameState == 2) {
            batch?.draw(dizzy, (Gdx.graphics.width / 2 - man[manState].width / 2).toFloat(), manY)
        } else {
            batch?.draw(man[manState], (Gdx.graphics.width / 2 - man[manState].width / 2).toFloat(), manY)
        }


        manRectangle = Rectangle((Gdx.graphics.width / 2 - man[manState].width / 2).toFloat(), manY, man[manState].width.toFloat(), man[manState].height.toFloat())
        for (i in coinRectangles.indices) {
            if (Intersector.overlaps(manRectangle, coinRectangles[i])) {
                score++
                coinRectangles.removeAt(i)
                coinXs.removeAt(i)
                coinYs.removeAt(i)
                break
            }
        }

        for (i in bombRectangles.indices) {
            if (Intersector.overlaps(manRectangle, bombRectangles[i])) {
                gameState = 2
            }
        }
        font.draw(batch, score.toString(), 100f, 200f)
        batch?.end()
    }

    override fun dispose() {
        batch!!.dispose()
    }
}